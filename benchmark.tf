# Configure the OpenStack Provider
provider "openstack" {
    user_name         = "stress@haven.vng.cloud"
    tenant_name       = "BVN000004 stress"
    password          = "xxxxxxxx"
    auth_url          = "https://identity.openstack.cloudvps.com/v3"
    region            = "AMS"
}

# Dashboard
resource "openstack_compute_instance_v2" "dashboard-001" {
    name      = "dashboard-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 4GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "dashboard"
    }
}

# Directory
resource "openstack_compute_instance_v2" "directory-001" {
    name      = "directory-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 1GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "directory"
    }
}

# Benchmark API
resource "openstack_compute_instance_v2" "benchmark-api-001" {
    name      = "benchmark-api-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 1GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_api"
    }
}
resource "openstack_compute_instance_v2" "benchmark-api-002" {
    name      = "benchmark-api-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Standard 1GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_api"
    }
}

# Inways
resource "openstack_compute_instance_v2" "inway-001" {
    name      = "inway-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 8GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "inway"
    }
}
resource "openstack_compute_instance_v2" "inway-002" {
    name      = "inway-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 8GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "inway"
    }
}

# TxLog
resource "openstack_blockstorage_volume_v3" "transaction-log-001" {
    name                = "transaction-log-001"
    availability_zone   = "AMS-EU4"
    description         = "transaction-log-001 db volume"
    volume_type         = "ssd"
    size                = 100
}

resource "openstack_compute_instance_v2" "transaction-log-001" {
    name      = "transaction-log-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 24GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "transaction_log"
    }
}

resource "openstack_compute_volume_attach_v2" "transaction-log-001" {
  instance_id = openstack_compute_instance_v2.transaction-log-001.id
  volume_id   = openstack_blockstorage_volume_v3.transaction-log-001.id
}

# Create outways
resource "openstack_compute_instance_v2" "outway-001" {
    name      = "outway-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 8GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "outway"
    }
}
resource "openstack_compute_instance_v2" "outway-002" {
    name      = "outway-002"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 8GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "outway"
    }
}

# Benchmark clients
resource "openstack_compute_instance_v2" "benchmark-client-001" {
    name      = "benchmark-client-001"
    image_id  = "a376e7fe-976f-43f5-ab70-f1b211a1f631"
    availability_zone = "AMS-EU4"
    flavor_name = "Small HD 24GB"
    key_pair  = "arnoud public rsa"
    security_groups = ["allow-all"]

    network {
        name = "net-public"
    }

    metadata = {
        environment = "benchmark"
        group       = "benchmark_client"
    }
}
