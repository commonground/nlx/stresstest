# NLX benchmark

Demonstrate a minimal but complete nlx network.

Minimum set of services:

- directory-registration-api
- directory-inspection-api
- database (for the directory)
- inway (to expose an api in the nlx network)
- outway (to consume data from api's out in the nlx network)
- benchmark api which only returns `200 OK`.
- *configure your own api in the inway!*

Benchmarking your own API behind NLX inway is
possible. Just add your service to the
`service-config.toml` the configuration


## Requirements

* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* [Terraform](https://www.terraform.io/)

## Usage

```bash
# Download terraform openstack plugin
terraform init

# Create the VMs
terraform apply

# Apply ansible
cd ansible; ansible-playbook benchmark.yaml -i openstack.yaml --ask-vault-pass -u root

# Destroy VMs
terraform destroy
```

### Dashboards

```bash
There are dashboards ready to import in the ansible/roles/grafana/files/dashboards directory.
```

### JMeter

In the `jmeter-tests` directory you can find a test plan to get started.
