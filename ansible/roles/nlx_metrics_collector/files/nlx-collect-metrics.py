#!/usr/bin/env python3
from typing import Optional

from datetime import datetime
from logging import getLogger

import requests
from influxdb import InfluxDBClient


ENVIRONMENTS = {
    "demo": "https://directory.demo.nlx.io",
    "pre-production": "https://directory.preprod.nlx.io",
    "production": "https://directory.prod.nlx.io",
}

INFLUX_HOSTNAME = "localhost"
INFLUX_DATABASE = "nlx"

log = getLogger(__name__)


def count_field(field: str, url: str) -> Optional[int]:
    response = requests.get(url)
    if not response.ok:
        log.warning("Request was not successful: {}".format(response.status_code))
        return None

    content = response.json()
    if field not in content:
        return 0

    return len(content[field])


def create_directory_measurement(environment: str, url: str, timestamp: datetime) -> Optional[dict]:
    organization_count = count_field("organizations", "{}/api/directory/list-organizations".format(url))
    service_count = count_field("services", "{}/api/directory/list-services".format(url))

    if organization_count is None or service_count is None:
        log.warning("Failed to collect metrics for '{}' environment".format(environment))
        return None

    measurement = {
        "measurement": "directory",
        "time": timestamp,
        "fields": {
            "organizations": organization_count,
            "services": service_count,
        },
        "tags": {
            "environment": environment,
        },
    }

    return measurement


def main():
    now = datetime.utcnow()
    measurements = []

    for environment, url in ENVIRONMENTS.items():
        measurement = create_directory_measurement(environment, url, now)
        if measurement is not None:
            measurements.append(measurement)

    influx_client = InfluxDBClient(host=INFLUX_HOSTNAME, database=INFLUX_DATABASE)
    influx_client.write_points(measurements, time_precision="s")


if __name__ == "__main__":
    main()
