# Ansible

## Add user

First generate a hash of the password you want to use for the user account. The Python package `passlib` is required.

```sh
python3 -c "from passlib.hash import sha512_crypt; import getpass; print(sha512_crypt.using(rounds=5000).hash(getpass.getpass()))"
```

Then edit the encypted file.

```sh
ansible-vault edit inventories/group_vars/all/common_users.yaml
```

Install the requirements:

```sh
pip install -r requirements.txt
```

Place a clouds.yaml file in your home directory with credentials:

```sh
mkdir -p ~/.config/openstack/
vi ~/.config/openstack/clouds.yaml
```

In this file, place the following yaml:

```
clouds:
  cloudvps:
    auth:
      username: blabla@blabla.cloud
      password: mypasswordhere
      tenant_name: "BVN000004 stress"
      auth_url: https://identity.openstack.cloudvps.com/v3
      user_domain_name: Default
      project_domain_name: default
    region_name: AMS
```

Run ansible to provision all the hosts:
```
ansible-playbook benchmark.yaml -i openstack.yaml --ask-vault-pass -u root
```

Run ansible to provision a subset of hosts:
```
ansible-playbook benchmark.yaml -i openstack.yaml --ask-vault-pass -u root -l directory,dashboard
```
