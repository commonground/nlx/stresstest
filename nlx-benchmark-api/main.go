package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
)

const listenAddress = ":3000"
const randomDataSize = 512 * 1024 // kilobyte
const randomDataReadSize = 10 * 1024

var randomData []byte

type authRequest struct {
	Headers      http.Header `json:"headers"`
	Organization string      `json:"organization"`
	Service      string      `json:"service"`
}

type authResponse struct {
	Authorized bool        `json:"authorized"`
	Headers    http.Header `json:"headers"`
	Reason     string      `json:"reason,omitempty"`
}

func main() {
	r := chi.NewRouter()

	r.HandleFunc("/", IndexRequestHandler)
	r.HandleFunc("/auth", AuthRequestHandler)
	r.HandleFunc("/data/{size}", DataRequestHandler)
	r.HandleFunc("/sleep/{duration}", SleepRequestHandler)
	r.HandleFunc("/sleep/{durationMin}-{durationMax}", RandomSleepRequestHandler)
	r.HandleFunc("/random", RandomRequestHandler)

	fillRandomDataBuffer(randomDataSize)

	log.Println("Listening on", listenAddress)

	http.ListenAndServe(listenAddress, r)
}

func fillRandomDataBuffer(size int) {
	randomData = make([]byte, size)
	rand.Read(randomData)
}

func AuthRequestHandler(w http.ResponseWriter, r *http.Request) {
	authRequest := &authRequest{}
	defer r.Body.Close()

	durationMax := 5000
	durationMin := 500
	durationRand := rand.Intn(durationMax-durationMin) + durationMin
	duration := time.Duration(durationRand) * time.Millisecond
	timer := time.NewTimer(duration)

	err := json.NewDecoder(r.Body).Decode(authRequest)
	if err != nil {
		log.Printf("error decoding request %s", err)
		http.Error(w, "error decoding request", http.StatusBadRequest)
		return
	}

	log.Printf("Received auth request organization '%s' service '%s' ", authRequest.Organization, authRequest.Service)

	<-timer.C

	err = json.NewEncoder(w).Encode(&authResponse{
		Authorized: true,
		Headers:    authRequest.Headers,
	})

	if err != nil {
		log.Print("error encoding auth response")
		http.Error(w, "error encoding auth response", http.StatusInternalServerError)
	}
}

func IndexRequestHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Ok\n"))
}

func DataRequestHandler(w http.ResponseWriter, r *http.Request) {
	sizeString := chi.URLParam(r, "size")

	size, _ := strconv.Atoi(sizeString)

	if size > randomDataSize {
		http.Error(w, "Size cannot exceed " + strconv.Itoa(randomDataSize), http.StatusBadRequest)
		return
	}

	w.Write(readRandomData(size))
}

func SleepRequestHandler(w http.ResponseWriter, r *http.Request) {
	durationString := chi.URLParam(r, "duration")

	duration, err := time.ParseDuration(durationString + "ms")
	if err != nil {
		log.Println("Failed to parse duration", durationString)
		http.Error(w, "Failed to parse duration", http.StatusBadRequest)
		return
	}

	respondAfter(duration, w)
}

func RandomSleepRequestHandler(w http.ResponseWriter, r *http.Request) {
	durationMinString := chi.URLParam(r, "durationMin")
	durationMaxString := chi.URLParam(r, "durationMax")

	durationMin, _ := strconv.Atoi(durationMinString)
	durationMax, _ := strconv.Atoi(durationMaxString)

	if durationMax <= durationMin {
		http.Error(w, "Max needs to be higher then min", http.StatusBadRequest)
		return
	}

	d := rand.Intn(durationMax-durationMin) + durationMin

	duration := time.Duration(d) * time.Millisecond

	respondAfter(duration, w)
}

func RandomRequestHandler(w http.ResponseWriter, r *http.Request) {
	w.Write(readRandomData(rand.Intn(randomDataReadSize)))
}

func respondAfter(duration time.Duration, w http.ResponseWriter) {
	timer := time.NewTimer(duration)
	data := readRandomData(rand.Intn(randomDataReadSize))

	<-timer.C

	w.Write(data)
}

func readRandomData(size int) []byte {
	pos := rand.Intn(randomDataSize)
	end := pos + size
	if end > randomDataSize {
		end = randomDataSize
	}

	return randomData[pos:end]
}
