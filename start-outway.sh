#!/usr/bin/env bash

PATH="${GOPATH:-${HOME}/go}/bin:${PATH}"

nlx-outway \
  --directory-inspection-address "directory-inspection-api:8091" \
  --listen-address "127.0.0.1:8071" \
  --listen-address-tls "127.0.0.1:8072" \
  --tls-nlx-root-cert "./tls/root.crt" \
  --tls-org-cert "./tls/inway.crt" \
  --tls-org-key "./tls/inway.key" \
  --disable-logdb \
  --log-type "local" \
  --log-level "warn"
